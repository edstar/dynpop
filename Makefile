wfig="https://www.lmd.jussieu.fr/~nvillefranque/edstar/dynpop/evolution_roffiac_effect_climat_et_numerique.png"
default: all

all: pdf

# extrait le tex du fichier noweb/main.nw et le met dans le fichier tex/main.tex
tex/main.tex: noweb/main.nw
	if [ ! -d tex ] ; then mkdir tex ; fi
	noweave -index -delay -v noweb/main.nw > tex/main.tex

# extrait le C du fichier noweb/main.nw et le met dans le fichier src/main.c
src/main.c: noweb/main.nw
	if [ ! -d src ] ; then mkdir src ; fi
	notangle -Rmain noweb/main.nw > src/main.c

# deux façons d'inclure des figures : source extérieure ou exécution du programme
figures/evolution_roffiac_effect_climat_et_numerique.png:
	if [ ! -d figures ] ; then mkdir figures ; fi
	if [ ! -f figures/evolution_roffiac_effect_climat_et_numerique.png ] ; then cd figures ; wget ${wfig} ; fi

local/evolution_roffiac_phase.png: install
	cd local && ./dynpop &&\
	echo "set nokey; set terminal png size 1280,920; set output 'evolution_roffiac_phase.png'; set xlabel 'nombre d habitants total'; set ylabel 'cout au metre carre'; plot 'evolution_roffiac.txt' u 2:3;" > gplt_roffiac_phase;\
	gnuplot < gplt_roffiac_phase

# pour compiler le pdf : besoin du fichier latex et des figures incluses dedans
pdf: tex/main.tex figures/evolution_roffiac_effect_climat_et_numerique.png  local/evolution_roffiac_phase.png
	cd tex && pdflatex main.tex && pdflatex main.tex && cd ..
	cp tex/main.pdf local/main.pdf

# pour compiler le programme : besoin du fichier source C
dynpop: src/main.c
	if [ ! -d build ] ; then mkdir build ; fi
	cd build && gcc ../src/main.c -o dynpop -lm

# créé un répertoire de travail et copie l'executable dedans
install: dynpop
	if [ ! -d local ] ; then mkdir -p local ; fi
	cp build/dynpop local/

clean:
	rm -fr src
	rm -fr tex
	rm -fr build
	rm -fr local
