#include <math.h>
#include <float.h>
#include <stdio.h>

struct parametres {
  float klin;
  float k1;
  float k2;
  float k3;
  float k4;
  float yp;
};

struct observations {
  int X0;
  float Y0;
  int Xobs;
  float Yobs;
};

struct donnees {
  struct parametres par;
  struct observations obs;
};

float calcul_x_spontane(float t, struct parametres* par, struct observations* obs) {
  float X0 = obs->X0;
  float k1 = par->k1;

  return exp(k1*t)*X0;
};

float calcul_dtx(float x, float y, struct parametres* par){
 float k1 = par->k1;
 float k2 = par->k2;
 return (k1 - k2*y)*x;
}

float calcul_dty(float x, float y, struct parametres* par){
  float k3 = par->k3;
  float k4 = par->k4;
  return (- k3 + k4*x)*y;
}

float calcul_y(float tmax, struct parametres* par, struct observations* obs, int model_x){
  int i;
  int Nt;
  float dt=.001;
  float tprec, tnext;       
  float yprec, ynext;       
  float xt;
  float dty;          /* augmentation de Y pendant un petit pas de temps */
  float yprevu;       /* coût prévu au bout d'une durée t */ 

  float Y0 = obs->Y0;
  float k3 = par->k3;
  float k4 = par->k4;

  tprec = 0;
  yprec = Y0;
  Nt = (int)(tmax/dt);
  for (i=1;i<Nt-1;i++) {
    tnext = tprec+dt; /* tmax/(float)(Nt-1)*i;*/
    if (model_x == 0) {
      NULL;
    } else if (model_x == 1) {
      xt = calcul_x_spontane(tprec, par, obs);
    }
    dty = calcul_dty(xt, yprec, par);
    ynext = yprec + dty*(tnext - tprec);
    yprec = ynext;
    tprec = tnext;
  }

  yprevu = ynext;
  return yprevu;
}

float 
estime_k4(float Yobs, struct parametres* par, struct observations* obs) {
  /* on fait l'hypothèse que l'évolution de x ne dépend pas encore de l'évolution des coûts */
  int model_x = 1;
  int i; 
  int Nk;       /* nombre de valeurs de k qu'on va tester */
  float Yprevu; /* coût prévu par le modèle étant donné un paramètre */
  float k3, k, kmin, kmax;  /* valeurs min et max qu'on va tester */
  float distmin, bestk; /* variables pour retenir le meilleur paramètre */
  float tobs=2.; /* année où on a une observation (2019 est l'année 0)  */
  float X0 = obs->X0; /* nombre d'habitants au temps 0 */

  Nk = 1000;
  kmin = 0.0005;
  kmax = 0.005;
 
  distmin = FLT_MAX;
  bestk = kmin;


  for (i=0;i<Nk;i++) {
    /* valeur de k à tester */
    k = kmin + (kmax-kmin)/(float)(Nk-1)*i;
    par->k4 = k  ;

    /* pour estimer k4 on fait l'hypothèse :*/
    k3 = par->k4 * X0;
    par->k3 = k3 ;

    /* coût prévu pour cette valeur de k */
    Yprevu = calcul_y(tobs, par, obs, model_x);

    /* est ce qu'elle est proche de l'observation ? */
    if (fabs(Yprevu-Yobs) < distmin) {
      bestk = k;
      distmin = fabs(Yprevu-Yobs);
    } 
  }

  return bestk;
}

int
calcul_xy_roffiac(float tmax, struct parametres* par, struct observations *obs, int*xt, float*yt){
  static int nb=0;
  int i, augmente=1, n_inversion=0;
  int n_step_in_period = 0 ;
  float x_moy_periode=0;
  float y_moy_periode=0;
  float Nt;
  float dt=.001; /* passer en dt pour éviter la divergence quelque soit le temps d'observation !!! */
  float tprec, tnext;
  float xprec, xnext, dtx;
  float x1prec, x1next, dtx1;
  float x2prec, x2next, dtx2;
  float yprec, ynext, dty;
  float k1,k2,k3,k4 ;
  float tau ;
  float kclimat, kretraite, kservice, knum, kdeces, kpaysage ;
  int X0 = obs->X0;
  int Y0 = obs->Y0;
  char filename[512];
  FILE* output;

  /* sprintf(filename, "toto%i",nb); */
  sprintf(filename, "evolution_roffiac.txt");

  output = fopen(filename, "w");
  fprintf(output, "# Calcul de l'évolution de X et Y en fonction de t\n");
  fprintf(output, "# Paramètre k2 = %f\n", par->k2);
  fprintf(output, "# t\tX(t)\tY(t)\tdX(t)\tdY(t)\n");

  k1 = par->k1;
  k2 = par->k2;
  k3 = par->k3;
  k4 = par->k4;
 
  tprec=0;
  xprec=X0;
  yprec=Y0;

  /* au debut, 30 % d'actifs */
  x1prec = .4*xprec ; /* non actifs */
  x2prec = .6*xprec ; /* actifs */

  Nt = tmax/dt;
  kclimat = 0.05 ;
  kretraite = 0.01 ; /* .01 ;*/
  tau = 5;

  for (i=1; i<Nt; i++) {
    knum = 10*(1 - exp(-tprec/tau)); 
    xprec = x1prec + x2prec;
    dtx1 = (  k1 - k2*yprec + .5*kclimat)*x1prec + kretraite*x2prec; /* non actifs */
    dtx2 = (  k1 - k2*yprec + kclimat)*x2prec - kretraite*x2prec + knum;    /* actifs */
    dty =  (- k3 + k4*(xprec))*yprec; 
    if (i%10==0) fprintf(output, "%f\t%f\t%f\t%f\t%f\t%f\t%f\n", tprec, xprec, yprec, x1prec, x2prec, dtx, dty);
    tnext = tprec + dt;
    x1next = x1prec + dtx1*(tnext-tprec);
    x2next = x2prec + dtx2*(tnext-tprec);
    xnext = x1next + x2next ;
    ynext = yprec + dty*(tnext-tprec);
    xprec = xnext;
    yprec = ynext;
    tprec = tnext;
    x1prec = x1next;
    x2prec = x2next;
  }

  *xt=xnext;
  *yt=ynext;
  nb = nb+1 ;
  fclose(output);
return 0;
}

int
main(int argc, char** argv)
{
  int i, err;
  int Xt;
  int Nk;
  float Yt;
  float dk;
  struct parametres par;
  struct observations obs;

  obs.X0   = 400  ;          /* nombre d'habitants à St Front à t0   = 2019 */
  obs.Xobs = 460  ;          /* nombre d'habitants à St Front à tobs = 2021 */
  obs.Y0   = 500. ;          /* prix du mètre carré           à t0   = 2019 */
  obs.Yobs = 1.14*(obs.Y0) ; /* prix du mètre carré           à tobs = 2021 */

  /* En fait plus il y a de monde et plus ça devient cher donc ça va ralentir */
  /* Il faut une variable pour caractériser le coût du m2, dont va dépendre
   * l'évolution de la population... nouvelle couche de modélisation */
  
  /* vitesse d'augmentation de la population */
  par.k1 = log((float)obs.Xobs/(float)obs.X0)/2.; 

  /* effet du nombre d'habitant sur l'augmentation du coût du mètre carré */
  par.k4 = estime_k4(obs.Yobs, &par, &obs); /* pour retrouver l'évolution de 2019 à 2021 */

  /* diminution spontanée du coût au mètre carré en l'absence de population */
  par.k3 = (par.k4)*(obs.X0); /* le coût n'augmentait pas en 2019 */

  /* choisi pour que les coûts ne dépassent pas 2000 euros au mètre carré */
  par.k2 = 0.000065 ;

  printf("k1 %f k2 %f k3 %f k4 %f\n", par.k1, par.k2, par.k3, par.k4);
  printf("Point fixe : Xf = k3/k4 = %f , Yf = k1/k2 = %f\n", par.k3/par.k4, par.k1/par.k2);

  calcul_xy_roffiac(100, &par, &obs, &Xt, &Yt);

exit:
  return 0;
error:
  err = -1; /* il y a eu une erreur */
  goto exit;
}
