#include <math.h>
#include <float.h>
#include <stdio.h>

int
main(int argc, char** argv)
{
  int i;
  float dY[100];
  float Y[100];
  float dX[100];
  float X[100];
  float kx; /* paramètre de croissance de la population */
  float ky; /* paramètre de décroissance des coûts */
  char filename[30];
  FILE* file;

  sprintf(filename, "evolution_exponentielle.txt");
  file=fopen(filename, "w");



  X[0] = 400 ;
  X[1] = 460 ;

  kx = 0.15 ;

  Y[0] = 500;
  Y[1] = 570;

  dY[0] = 0;
  
  ky = 0.14 ;

  for (i=1 ; i < 30 ; i++) { 

    dX[i] = kx * X[i];       

    X[i+1] = X[i] + dX[i] ;  

    ky = 0.14*(X[i]-400.)/60.; 

    dY[i] = ky*Y[i] ;

    Y[i+1] = Y[i] + dY[i] ;

    fprintf(file,"%i\t%i\t%f\n", 2019+2*i, (int)X[i], Y[i]);
  }

fclose(file);
return 0;
}
